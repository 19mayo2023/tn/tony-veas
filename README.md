# Tony Veas

## Video Demostrativo

v2.0 - Para una mejor comprensión del proyecto, le invito a ver el siguiente video:

[![Demo Video](http://img.youtube.com/vi/IJ6j9_LiDL0/0.jpg)](https://www.youtube.com/watch?v=IJ6j9_LiDL0 "Video Title")

v1.0 - Para una mejor comprensión del proyecto, le invito a ver el siguiente video:

[![Demo Video](http://img.youtube.com/vi/FAdv_QGQDn4/0.jpg)](http://www.youtube.com/watch?v=FAdv_QGQDn4 "Video Title")
