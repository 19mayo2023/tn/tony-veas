import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AdministracionRolesComponent } from './views/administracion-roles/administracion-roles.component';
import { AdministracionUsuariosComponent } from './views/administracion-usuarios/administracion-usuarios.component';
import { FormularioRolComponent } from './views/formulario-rol/formulario-rol.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'administracion-roles', component: AdministracionRolesComponent },
  {
    path: 'administracion-usuarios',
    component: AdministracionUsuariosComponent,
  },
  {
    path: 'page-rol',
    component: FormularioRolComponent,
  },
  {
    path: 'page-rol/:id',
    component: FormularioRolComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
