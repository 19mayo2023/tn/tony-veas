import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(private router: Router) {}

  logout() {
    localStorage.removeItem('ACCESS_TOKEN');
    this.router.navigate(['/login']);
  }

  goBackToDashboard() {
    this.router.navigate(['/dashboard']);
  }

  goBackToAdministracionRoles() {
    this.router.navigate(['/administracion-roles']);
  }

  goBackToAdministracionUsuarios() {
    this.router.navigate(['/administracion-usuarios']);
  }
}
