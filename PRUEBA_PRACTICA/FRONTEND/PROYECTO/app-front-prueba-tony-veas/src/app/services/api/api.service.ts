import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl = 'http://localhost:8080/profile';
  private urlBase = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  getProfile(): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    console.log(token);
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.get(this.apiUrl, httpOptions);
  }

  getAllRoles(): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.get<any>(`${this.urlBase}/roles`, httpOptions);
  }

  getRoleById(id: string): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.get<any>(`${this.urlBase}/roles/${id}`, httpOptions);
  }

  createRole(role: any): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.post<any>(`${this.urlBase}/roles`, role, httpOptions);
  }

  updateRole(id: string, updatedRole: any): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.put<any>(
      `${this.urlBase}/roles/${id}`,
      updatedRole,
      httpOptions
    );
  }

  deleteRole(id: number): Observable<void> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.delete<void>(`${this.urlBase}/roles/${id}`, httpOptions);
  }

  getAllUsersWithRoles(): Observable<any> {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (!token) {
      console.error('No token found!');
    }

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: `Bearer ${token}` }),
    };

    return this.http.get<any>(`${this.urlBase}/usuarios-roles`, httpOptions);
  }
}
