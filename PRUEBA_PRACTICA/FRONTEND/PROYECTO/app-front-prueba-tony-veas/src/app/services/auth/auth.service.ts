import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  AUTH_SERVER: string = 'http://localhost:8080';
  authSubject = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient) {}

  login(user: any): Observable<any> {
    console.log('URL: ', `${this.AUTH_SERVER}/authenticate`);
    console.log('user: ', user);
    return this.httpClient.post(`${this.AUTH_SERVER}/authenticate`, user).pipe(
      tap((res: any) => {
        if (res.jwt) {
          localStorage.setItem('ACCESS_TOKEN', res.jwt);
          this.authSubject.next(true);
        }
      })
    );
  }
}
