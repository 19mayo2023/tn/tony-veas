import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-administracion-roles',
  templateUrl: './administracion-roles.component.html',
  styleUrls: ['./administracion-roles.component.css'],
})
export class AdministracionRolesComponent {
  roles: any = [];

  constructor(private router: Router, private apiService: ApiService) {}

  ngOnInit() {
    this.getRols();
  }

  crearRol() {
    this.router.navigate(['/page-rol']);
  }

  editRol(rolId: number) {
    this.router.navigate(['/page-rol', rolId]);
  }

  eliminarRol(id: any): void {
    console.log(`Eliminar el rol: ${id}`);

    console.log('Delete: ', id);
    this.apiService.deleteRole(id).subscribe((res) => {
      console.log(res);
      this.getRols();
    });
  }

  getRols() {
    this.apiService.getAllRoles().subscribe(
      (data) => {
        console.log(data);
        this.roles = data;
      },
      (error) => console.log(error)
    );
  }
}
