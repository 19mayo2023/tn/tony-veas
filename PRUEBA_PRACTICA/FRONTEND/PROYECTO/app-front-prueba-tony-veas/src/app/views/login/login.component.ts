import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: '',
    });

    const authToken = localStorage.getItem('ACCESS_TOKEN');
    if (authToken) {
      this.router.navigate(['/dashboard']);
    }
  }

  ngOnInit(): void {}

  onSubmit() {
    if (this.loginForm.valid) {
      const val = this.loginForm.value;

      this.authService.login(val).subscribe(
        () => {
          console.log('Usuario autenticado exitosamente');
          this.router.navigate(['/dashboard']);
        },
        (error) => {
          alert(
            'Por favor, vuelva a intentarlo. Caso contrario intente más tarde'
          );
        }
      );
    }
  }

  goToRegister(): void {
    this.router.navigate(['/register']);
  }
}
