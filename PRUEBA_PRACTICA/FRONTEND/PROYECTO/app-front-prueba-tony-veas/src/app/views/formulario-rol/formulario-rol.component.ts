import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
@Component({
  selector: 'app-formulario-rol',
  templateUrl: './formulario-rol.component.html',
  styleUrls: ['./formulario-rol.component.css'],
})
export class FormularioRolComponent {
  roleForm: FormGroup;
  editMode = false;
  roleId!: string | null;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.roleForm = this.fb.group({
      id: [{ value: '', disabled: this.editMode }],
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params) => {
      if (params.has('id')) {
        this.editMode = true;
        this.roleId = params.get('id');
        this.apiService.getRoleById(this.roleId!).subscribe((res) => {
          console.log('RES: ', res);
          this.roleForm.patchValue(res);
        });
        console.log('Editando id: ', this.roleId);
      } else {
        console.log('Creando rol');
      }
    });
  }

  onSubmit() {
    if (this.editMode) {
      const rolData = { ...this.roleForm.value, id: this.roleId };
      console.log('Descendence: ', rolData);
      this.apiService.updateRole(this.roleId!, rolData).subscribe(
        (res) => {
          console.log('ressssssssssssssssssss: ', res);
          this.router.navigate(['/administracion-roles']);
        },
        (err) => {
          console.log('Err');
          alert(
            'Error: vuelva a intentar o intente más tarde. Nota: No se puede editar el rol ADMINISTRADOR'
          );
        }
      );
      console.log('Editando');
    } else {
      const rolData = this.roleForm.value;
      console.log('Creando: ', rolData);
      this.apiService.createRole(rolData).subscribe(
        (res) => {
          console.log('resRol: ', res);
          this.router.navigate(['/administracion-roles']);
        },
        (err) => {
          alert('Error intente de nuevo y verifique sus datos');
        }
      );
      console.log('Creando');
      console.log(this.roleForm);
    }
  }
}
