import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-administracion-usuarios',
  templateUrl: './administracion-usuarios.component.html',
  styleUrls: ['./administracion-usuarios.component.css'],
})
export class AdministracionUsuariosComponent {
  usersWithRoles: any = [];

  constructor(private router: Router, private apiService: ApiService) {}

  ngOnInit(): void {
    this.getUsersWithRoles();
  }

  onEdit(role: any): void {
    console.log(`Editar el rol: ${role.name}`);
  }

  onDelete(role: any): void {
    console.log(`Eliminar el rol: ${role.name}`);
  }

  getUsersWithRoles() {
    this.apiService.getAllUsersWithRoles().subscribe(
      (data) => {
        console.log(data);
        this.usersWithRoles = data;
      },
      (error) => console.log(error)
    );
  }
}
