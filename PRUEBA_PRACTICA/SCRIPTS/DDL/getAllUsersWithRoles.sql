USE gestion_roles;
DROP PROCEDURE IF EXISTS getAllUsersWithRoles;
DELIMITER //
CREATE PROCEDURE getAllUsersWithRoles()
BEGIN
    SELECT admin_usuario.*, admin_rol.nombre as rol_nombre 
    FROM admin_usuario
    LEFT JOIN info_usuario_rol ON admin_usuario.id = info_usuario_rol.usuario_id
    LEFT JOIN admin_rol ON info_usuario_rol.rol_id = admin_rol.id;
END //
DELIMITER ;
CALL GetAllUsersWithRoles();