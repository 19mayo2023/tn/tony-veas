# Creación de la base de datos
CREATE DATABASE IF NOT EXISTS gestion_roles;
USE gestion_roles;

# Creación de la tabla admin_rol
CREATE TABLE IF NOT EXISTS `admin_rol` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(255) NOT NULL,
    `descripcion` VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# Creación de la tabla admin_usuario
CREATE TABLE IF NOT EXISTS `admin_usuario` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `usuario` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `nombres` VARCHAR(255),
    `apellidos` VARCHAR(255),
    `direccion` VARCHAR(255),
    `telefono` VARCHAR(20),
    `usuario_creacion` VARCHAR(255),
    `fecha_creacion` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `foto` BLOB,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# Creación de la tabla info_usuario_rol
CREATE TABLE IF NOT EXISTS `info_usuario_rol` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `usuario_id` INT NOT NULL,
    `rol_id` INT NOT NULL,
    `usuario_creacion` VARCHAR(255),
    `fecha_creacion` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `usuario_modificacion` VARCHAR(255),
    `fecha_modificacion` TIMESTAMP,
    `estado` ENUM('ACTIVO', 'INACTIVO') NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`usuario_id`) REFERENCES `admin_usuario`(`id`),
    FOREIGN KEY (`rol_id`) REFERENCES `admin_rol`(`id`)
) ENGINE=InnoDB;
