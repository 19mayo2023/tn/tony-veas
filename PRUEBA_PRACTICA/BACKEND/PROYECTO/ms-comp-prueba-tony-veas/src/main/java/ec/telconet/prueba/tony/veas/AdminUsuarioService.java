package ec.telconet.prueba.tony.veas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;
import ec.telconet.prueba.tony.veas.jpa.AdminUsuarioRepository;

@Service
public class AdminUsuarioService {

	@Autowired
	private AdminUsuarioRepository adminUsuarioRepository;
	
	public AdminUsuario getUserByUsername(String username) {
        return adminUsuarioRepository.findByUsuario(username);
    }
	
}
