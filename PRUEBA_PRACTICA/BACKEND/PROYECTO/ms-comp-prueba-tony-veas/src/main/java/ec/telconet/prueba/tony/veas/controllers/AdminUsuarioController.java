package ec.telconet.prueba.tony.veas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.tony.veas.AdminUsuarioService;
import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;

@RestController
public class AdminUsuarioController {

	@Autowired
	private AdminUsuarioService adminUsuarioService;
	
	@GetMapping("/profile")
    public ResponseEntity<?> getProfile(@AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails == null) {
            return ResponseEntity.ok("No UserDetails present");
        } else {
            AdminUsuario user = adminUsuarioService.getUserByUsername(userDetails.getUsername());
            return ResponseEntity.ok(user);
        }
    }
	
}
