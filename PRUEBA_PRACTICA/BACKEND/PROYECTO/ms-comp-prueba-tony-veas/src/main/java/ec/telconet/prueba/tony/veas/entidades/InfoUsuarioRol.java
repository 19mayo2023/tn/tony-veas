package ec.telconet.prueba.tony.veas.entidades;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "info_usuario_rol")
public class InfoUsuarioRol {

	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "usuario_id", nullable = false)
	private AdminUsuario adminUsuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "rol_id", nullable = false)
	private AdminRol adminRol;

	private String usuarioCreacion;

	@Column(name = "fecha_creacion", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private LocalDateTime fechaCreacion;

	private String usuarioModificacion;
	private LocalDateTime fechaModificacion;

	@Enumerated(EnumType.STRING)
	private Estado estado;

	// Getters and setters

	public enum Estado {
		ACTIVO, INACTIVO
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AdminUsuario getAdminUsuario() {
		return adminUsuario;
	}

	public void setAdminUsuario(AdminUsuario adminUsuario) {
		this.adminUsuario = adminUsuario;
	}

	public AdminRol getAdminRol() {
		return adminRol;
	}

	public void setAdminRol(AdminRol adminRol) {
		this.adminRol = adminRol;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "InfoUsuarioRol [id=" + id + ", adminUsuario=" + adminUsuario + ", adminRol=" + adminRol
				+ ", usuarioCreacion=" + usuarioCreacion + ", fechaCreacion=" + fechaCreacion + ", usuarioModificacion="
				+ usuarioModificacion + ", fechaModificacion=" + fechaModificacion + ", estado=" + estado + "]";
	}

}
