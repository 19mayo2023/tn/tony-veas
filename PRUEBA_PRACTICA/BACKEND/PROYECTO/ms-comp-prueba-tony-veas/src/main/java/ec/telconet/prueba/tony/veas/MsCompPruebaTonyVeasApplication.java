package ec.telconet.prueba.tony.veas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaTonyVeasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaTonyVeasApplication.class, args);
	}

}
