package ec.telconet.prueba.tony.veas.entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "admin_rol")
public class AdminRol {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(nullable = false)
	private String nombre;

	private String descripcion;

	@OneToMany(mappedBy = "adminRol")
	@JsonIgnore
	private List<InfoUsuarioRol> infoUsuarioRol;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<InfoUsuarioRol> getInfoUsuarioRol() {
		return infoUsuarioRol;
	}

	public void setInfoUsuarioRol(List<InfoUsuarioRol> infoUsuarioRol) {
		this.infoUsuarioRol = infoUsuarioRol;
	}

	@Override
	public String toString() {
		return "AdminRol [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", infoUsuarioRol="
				+ infoUsuarioRol + "]";
	}

}
