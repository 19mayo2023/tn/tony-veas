package ec.telconet.prueba.tony.veas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.tony.veas.entidades.AdminRol;
import ec.telconet.prueba.tony.veas.jpa.AdminRolRepository;

@RestController
public class AdminRolController {

	@Autowired
    private AdminRolRepository adminRolRepository;

	@GetMapping("/roles")
    public List<AdminRol> getAllRoles() {
        return adminRolRepository.findAll();
    }
	
	@GetMapping("/roles/{id}")
    public ResponseEntity<AdminRol> getRoleById(@PathVariable Integer id) {
        AdminRol role = adminRolRepository.findById(id).orElse(null);
        if (role != null) {
            return ResponseEntity.ok(role);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@PostMapping("/roles")
    public ResponseEntity<AdminRol> createRole(@RequestBody AdminRol role) {
        AdminRol createdRole = adminRolRepository.save(role);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdRole);
    }
	
	@PutMapping("/roles/{id}")
	public ResponseEntity<AdminRol> updateRole(@PathVariable Integer id, @RequestBody AdminRol updatedRole) {
	    AdminRol role = adminRolRepository.findById(id).orElse(null);
	    if (role != null) {
	        if (!role.getNombre().equals("ADMINISTRADOR")) {
	            updatedRole.setId(id);
	            AdminRol savedRole = adminRolRepository.save(updatedRole);
	            return ResponseEntity.ok(savedRole);
	        } else {
	            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
	        }
	    } else {
	        return ResponseEntity.notFound().build();
	    }
	}

	@DeleteMapping("/roles/{id}")
	public ResponseEntity<Void> deleteRole(@PathVariable Integer id) {
	    AdminRol role = adminRolRepository.findById(id).orElse(null);
	    if (role != null) {
	        if (!role.getNombre().equals("ADMINISTRADOR")) {
	            adminRolRepository.deleteById(id);
	            return ResponseEntity.noContent().build();
	        } else {
	            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
	        }
	    } else {
	        return ResponseEntity.notFound().build();
	    }
	}
}
