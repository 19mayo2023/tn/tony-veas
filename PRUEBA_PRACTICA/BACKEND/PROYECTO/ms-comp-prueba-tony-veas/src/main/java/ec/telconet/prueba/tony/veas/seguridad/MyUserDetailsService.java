package ec.telconet.prueba.tony.veas.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;
import ec.telconet.prueba.tony.veas.jpa.AdminUsuarioRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private AdminUsuarioRepository adminUsuarioRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AdminUsuario adminUsuario = adminUsuarioRepository.findByUsuario(username);
        if (adminUsuario == null) {
            throw new UsernameNotFoundException("Usuario no encontrado");
        }

        return User.withUsername(adminUsuario.getUsuario()).password(adminUsuario.getPassword()).authorities("USER").build();
    }
}