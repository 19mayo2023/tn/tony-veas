package ec.telconet.prueba.tony.veas.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import ec.telconet.prueba.tony.veas.entidades.AdminRol;
import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;
import ec.telconet.prueba.tony.veas.entidades.InfoUsuarioRol;
import ec.telconet.prueba.tony.veas.entidades.InfoUsuarioRol.Estado;
import ec.telconet.prueba.tony.veas.jpa.AdminRolRepository;
import ec.telconet.prueba.tony.veas.jpa.AdminUsuarioRepository;
import ec.telconet.prueba.tony.veas.jpa.InfoUsuarioRolRepository;

@Component
public class DatabaseSeeder implements CommandLineRunner {

    private final AdminRolRepository adminRolRepository;
    private final AdminUsuarioRepository adminUsuarioRepository;
    private final InfoUsuarioRolRepository infoUsuarioRolRepository;

    public DatabaseSeeder(AdminRolRepository adminRolRepository, AdminUsuarioRepository adminUsuarioRepository, InfoUsuarioRolRepository infoUsuarioRolRepository) {
        this.adminRolRepository = adminRolRepository;
        this.adminUsuarioRepository = adminUsuarioRepository;
        this.infoUsuarioRolRepository = infoUsuarioRolRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // Definición de los roles
        String[] roles = { "EDITOR", "CONSULTOR", "ADMINISTRADOR" };

        // Información de los nuevos usuarios
        String[][] users = {
            { "editor@prueba.com", "Editor", "Apellidos Editor", "Dirección Editor", "Teléfono Editor" },
            { "consultor@prueba.com", "Consultor", "Apellidos Consultor", "Dirección Consultor", "Teléfono Consultor" },
            { "administrador@prueba.com", "Stefanny", "A.", "Avenida de las Américas", "Teléfono Administrador" }
        };

        for (int i = 0; i < roles.length; i++) {
            // Crea el rol si no existe
            AdminRol role = adminRolRepository.findByNombre(roles[i]);
            if (role == null) {
                role = new AdminRol();
                role.setNombre(roles[i]);
                role.setDescripcion("Rol de " + roles[i]);
                adminRolRepository.save(role);
            }

            // Crea el usuario si no existe
            AdminUsuario user = adminUsuarioRepository.findByUsuario(users[i][0]);
            if (user == null) {
                user = new AdminUsuario();
                user.setUsuario(users[i][0]);
                user.setPassword("contraseña_encriptada_" + roles[i]); // Deberías encriptar la contraseña
                user.setNombres(users[i][1]);
                user.setApellidos(users[i][2]);
                user.setDireccion(users[i][3]);
                user.setTelefono(users[i][4]);
                user.setUsuarioCreacion(users[i][0]);
                adminUsuarioRepository.save(user);
            }

            // Asocia el rol al usuario si no está ya asociado
            InfoUsuarioRol userRole = infoUsuarioRolRepository.findByAdminUsuarioAndAdminRol(user, role);
            if (userRole == null) {
                userRole = new InfoUsuarioRol();
                userRole.setAdminUsuario(user);
                userRole.setAdminRol(role);
                userRole.setUsuarioCreacion(users[i][0]);
                userRole.setEstado(Estado.ACTIVO);
                infoUsuarioRolRepository.save(userRole);
            }
        }
    }

}