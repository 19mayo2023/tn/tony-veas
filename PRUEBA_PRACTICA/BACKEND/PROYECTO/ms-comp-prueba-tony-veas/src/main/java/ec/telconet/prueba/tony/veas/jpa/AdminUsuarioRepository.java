package ec.telconet.prueba.tony.veas.jpa;

import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminUsuarioRepository extends JpaRepository<AdminUsuario, Integer> {
    AdminUsuario findByUsuario(String usuario);
}