package ec.telconet.prueba.tony.veas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.tony.veas.UsuarioRolDTO;
import ec.telconet.prueba.tony.veas.sps.UsuarioRolRepository;

@RestController
public class UsuarioRolController {

    private final UsuarioRolRepository usuarioRolRepository;

    @Autowired
    public UsuarioRolController(UsuarioRolRepository usuarioRolRepository) {
        this.usuarioRolRepository = usuarioRolRepository;
    }

    @GetMapping("/usuarios-roles")
    public List<UsuarioRolDTO> getAllUsersWithRoles() {
        return usuarioRolRepository.getAllUsersWithRoles();
    }
}
