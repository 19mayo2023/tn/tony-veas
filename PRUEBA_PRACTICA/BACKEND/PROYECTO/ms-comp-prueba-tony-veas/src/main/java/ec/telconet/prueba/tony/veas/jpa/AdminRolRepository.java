package ec.telconet.prueba.tony.veas.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.telconet.prueba.tony.veas.entidades.AdminRol;

public interface AdminRolRepository extends JpaRepository<AdminRol, Integer> {
    AdminRol findByNombre(String nombre);
}