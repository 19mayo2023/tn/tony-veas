package ec.telconet.prueba.tony.veas;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/mensaje")
	public String mensajeInicial() {
		return "App start";
	}
	
}
