package ec.telconet.prueba.tony.veas;

public class UsuarioRolDTO {
	private Integer usuarioId;
    private String usuarioApellidos;
    private String usuarioDireccion;
    private String usuarioNombres;
    private String nombreRol;
	public Integer getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getUsuarioApellidos() {
		return usuarioApellidos;
	}
	public void setUsuarioApellidos(String usuarioApellidos) {
		this.usuarioApellidos = usuarioApellidos;
	}
	public String getUsuarioDireccion() {
		return usuarioDireccion;
	}
	public void setUsuarioDireccion(String usuarioDireccion) {
		this.usuarioDireccion = usuarioDireccion;
	}
	public String getUsuarioNombres() {
		return usuarioNombres;
	}
	public void setUsuarioNombres(String usuarioNombres) {
		this.usuarioNombres = usuarioNombres;
	}
	public String getNombreRol() {
		return nombreRol;
	}
	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}
	@Override
	public String toString() {
		return "UsuarioRolDTO [usuarioId=" + usuarioId + ", usuarioApellidos=" + usuarioApellidos
				+ ", usuarioDireccion=" + usuarioDireccion + ", usuarioNombres=" + usuarioNombres + ", nombreRol="
				+ nombreRol + "]";
	}
    
    
    

}
