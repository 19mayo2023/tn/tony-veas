package ec.telconet.prueba.tony.veas.sps;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;

import ec.telconet.prueba.tony.veas.UsuarioRolDTO;

@Repository
public class UsuarioRolRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<UsuarioRolDTO> getAllUsersWithRoles() {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("GetAllUsersWithRoles");
        @SuppressWarnings("unchecked")
        List<Object[]> rawData = storedProcedure.getResultList();
        List<UsuarioRolDTO> results = new ArrayList<>();

        for(Object[] item : rawData) {
            UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
            usuarioRolDTO.setUsuarioId((Integer) item[0]);
            usuarioRolDTO.setUsuarioApellidos((String) item[1]);
            usuarioRolDTO.setUsuarioDireccion((String) item[2]);

            usuarioRolDTO.setUsuarioNombres((String) item[5]);
            usuarioRolDTO.setNombreRol((String) item[10]);
            results.add(usuarioRolDTO);
        }

        return results;
    }
}
