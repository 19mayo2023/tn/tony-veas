package ec.telconet.prueba.tony.veas.entidades;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "admin_usuario")
public class AdminUsuario {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(nullable = false)
	private String usuario;

	@Column(nullable = false)
	private String password;

	private String nombres;
	private String apellidos;
	private String direccion;
	private String telefono;
	private String usuarioCreacion;

	@Column(name = "fecha_creacion", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private LocalDateTime fechaCreacion;

	@Lob
	private byte[] foto;

	@OneToMany(mappedBy = "adminUsuario")
	@JsonIgnore
	private List<InfoUsuarioRol> infoUsuarioRol;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public List<InfoUsuarioRol> getInfoUsuarioRol() {
		return infoUsuarioRol;
	}

	public void setInfoUsuarioRol(List<InfoUsuarioRol> infoUsuarioRol) {
		this.infoUsuarioRol = infoUsuarioRol;
	}

	@Override
	public String toString() {
		return "AdminUsuario [id=" + id + ", usuario=" + usuario + ", password=" + password + ", nombres=" + nombres
				+ ", apellidos=" + apellidos + ", direccion=" + direccion + ", telefono=" + telefono
				+ ", usuarioCreacion=" + usuarioCreacion + ", fechaCreacion=" + fechaCreacion + ", foto="
				+ Arrays.toString(foto) + ", infoUsuarioRol=" + infoUsuarioRol + "]";
	}

}
