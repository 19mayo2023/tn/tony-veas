package ec.telconet.prueba.tony.veas.jpa;

import ec.telconet.prueba.tony.veas.entidades.AdminRol;
import ec.telconet.prueba.tony.veas.entidades.AdminUsuario;
import ec.telconet.prueba.tony.veas.entidades.InfoUsuarioRol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfoUsuarioRolRepository extends JpaRepository<InfoUsuarioRol, Integer> {
    InfoUsuarioRol findByAdminUsuarioAndAdminRol(AdminUsuario adminUsuario, AdminRol adminRol);
}