package ec.telconet.prueba.teorica.algoritmo1.tony.veas;

import java.util.Arrays;

public class OcurrenciasArreglo {

	// private int[] myArray = {1, 2, 2, 5, 4, 6, 7, 8, 8, 8};

	private int[] myArray;

	public OcurrenciasArreglo(int[] myArray) {
		super();
		this.myArray = myArray;
	}

	public void buscarOcurenciasEnArreglo() {
		
		// Variables auxiliares para buscar las ocurrencias
		int ocurrenciasMaximas = 0, 
			numeroMaximo = 0,
			actualesConcurrencias = 1,
			actualNumeroArreglo = myArray[0],
			longitudArreglo = myArray.length;

		// Realizamos la búsqueda en el arreglo
        for (int index = 1; index < longitudArreglo; index++) {
            if (myArray[index] == actualNumeroArreglo) {
            	actualesConcurrencias++;
            } else {
                if (actualesConcurrencias > ocurrenciasMaximas) {
                	ocurrenciasMaximas = actualesConcurrencias;
                	numeroMaximo = actualNumeroArreglo;
                }
                actualNumeroArreglo = myArray[index];
                actualesConcurrencias = 1;
            }
        }

        if (actualesConcurrencias > ocurrenciasMaximas) {
        	ocurrenciasMaximas = actualesConcurrencias;
        	numeroMaximo = actualNumeroArreglo;
        }

        // Resultado Final
        System.out.println("Recurrencias: " + ocurrenciasMaximas);
        System.out.println("Número: " + numeroMaximo);
        		
	}
	
	public int[] getMyArray() {
		return myArray;
	}

	public void setMyArray(int[] myArray) {
		this.myArray = myArray;
	}

	@Override
	public String toString() {
		return "OcurrenciasArreglo [myArray=" + Arrays.toString(myArray) + "]";
	}

}
