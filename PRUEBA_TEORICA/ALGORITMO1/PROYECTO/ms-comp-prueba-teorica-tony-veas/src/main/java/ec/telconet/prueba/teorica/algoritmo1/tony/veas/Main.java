package ec.telconet.prueba.teorica.algoritmo1.tony.veas;

public class Main {

	public static void main(String[] args) {

		int[] myArray = {1, 2, 2, 5, 4, 6, 7, 8, 8, 8, 0, 4, 4, 4, 4};
		// Creamos una instancia de la clase
		OcurrenciasArreglo ocurrenciasArreglo = new OcurrenciasArreglo(myArray);
		// Llamamos al método para encontrar las ocurrencias
		ocurrenciasArreglo.buscarOcurenciasEnArreglo();
	}

}
