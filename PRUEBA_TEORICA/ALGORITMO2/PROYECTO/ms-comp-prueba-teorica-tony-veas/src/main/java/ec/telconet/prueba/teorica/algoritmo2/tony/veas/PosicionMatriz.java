package ec.telconet.prueba.teorica.algoritmo2.tony.veas;

import java.util.Arrays;

public class PosicionMatriz {

	private int[] myArray;

	public PosicionMatriz(int[] myArray) {
		super();
		this.myArray = myArray;
	}

	public void imprimirPosicionFinalArray() {
		char[][] matriz = new char[4][4];

		for (int i = 0; i < 4; i++) {
		    Arrays.fill(matriz[i], 'O');
		}
		
        int posicionX = 0;
        int posicionY = 0;

        for (int index = 0; index < this.myArray.length; index += 2) {
            posicionX += this.myArray[index];
            posicionY += this.myArray[index + 1];

            if (posicionX < 0) {
            	posicionX = 0;
            } else if (posicionX > 3) {
            	posicionX = 3;
            }

            if (posicionY < 0) {
            	posicionY = 0;
            } else if (posicionY > 3) {
            	posicionY = 3;
            }
        }

        matriz[posicionY][posicionX] = 'X';

        for (int index = 0; index < 4; index++) {
            for (int j = 0; j < 4; j++) {
            	System.out.print(matriz[index][j]);
            }
            System.out.println();
        }
		
	}
	
	public int[] getMyArray() {
		return myArray;
	}

	public void setMyArray(int[] myArray) {
		this.myArray = myArray;
	}

	@Override
	public String toString() {
		return "PosicionMatriz [myArray=" + Arrays.toString(myArray) + "]";
	}
	
	
}
